package fornecedores.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fornecedores.dto.PedidoDTO;
import fornecedores.model.Pedido;
import fornecedores.repository.PedidoRepository;

@Service
public class PedidoService {
	
	@Autowired
	private PedidoRepository pedidoRepository;
	
	//Get
	public PedidoDTO findById(Long id) {
		Optional<Pedido> oPedido = pedidoRepository.findById(id);
		if(oPedido != null && oPedido.isPresent()) {
			Pedido pedido = oPedido.get();
			PedidoDTO pedidoDTO = new PedidoDTO(pedido.getId(), 
										pedido.getDescricao(), 
										pedido.getProduto(), 
										pedido.getCliente(), 
										pedido.getQtd(), 
										pedido.getValorTotal());
			return pedidoDTO;
		} else {
			return null;
		}
	}
	
	//Post
	public PedidoDTO savePedido(PedidoDTO pedidoDTO) {
		Pedido pedido = new Pedido(pedidoDTO.getId(), 
				pedidoDTO.getDescricao(), 
				pedidoDTO.getProduto(), 
				pedidoDTO.getCliente(), 
				pedidoDTO.getQtd(), 
				pedidoDTO.getValorTotal());
		
		if(pedidoDTO.getQtd() <= pedidoDTO.getProduto().getEstoque()) {
			
		pedido = pedidoRepository.save(pedido);
		pedidoDTO.setId(pedido.getId());
		return pedidoDTO;
		}else {
			return null;
		}
	}
	
	
	
	public void delete(Long id) {
		pedidoRepository.deleteById(id);
		}
		
	public PedidoDTO editProduto (Long id) {
		PedidoDTO pedidoDTO = new PedidoDTO();
			Pedido pedido = new Pedido(pedidoDTO.getId(), 
					pedidoDTO.getDescricao(), 
					pedidoDTO.getProduto(), 
					pedidoDTO.getCliente(), 
					pedidoDTO.getQtd(), 
					pedidoDTO.getValorTotal());
			pedido = pedidoRepository.save(pedido);
			return pedidoDTO;
		}

	
	

}
