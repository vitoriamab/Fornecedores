package fornecedores.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fornecedores.dto.FornecedorDTO;
import fornecedores.model.Fornecedor;
import fornecedores.repository.FornecedorRepository;

@Service
public class FornecedorService {
	
	@Autowired
	private FornecedorRepository fornecedorRepository;
	
	public FornecedorDTO findById(Long id) {
		Optional<Fornecedor> oFornecedor = fornecedorRepository.findById(id);
		
		if(oFornecedor != null && oFornecedor.isPresent()) {
			Fornecedor fornecedor = oFornecedor.get();
			FornecedorDTO fornecedorDTO = new FornecedorDTO(fornecedor.getId(), fornecedor.getNome(), fornecedor.getRazao_social(), fornecedor.getCnpj(), fornecedor.getEndereco());
			return fornecedorDTO;
		} else {
			return null;
		}
	}
	
	public FornecedorDTO saveFornecedor(FornecedorDTO fornecedorDTO) {
		Fornecedor fornecedor = new Fornecedor(fornecedorDTO.getId(), fornecedorDTO.getNome(), fornecedorDTO.getRazao_social(), fornecedorDTO.getCnpj(), fornecedorDTO.getEndereco());
		fornecedor = fornecedorRepository.save(fornecedor);
		fornecedorDTO.setId(fornecedor.getId());
		return fornecedorDTO;
	}
}
