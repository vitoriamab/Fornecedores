package fornecedores.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fornecedores.dto.ProdutoDTO;
import fornecedores.model.Produto;
import fornecedores.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	//GET
	public ProdutoDTO findById(Long id) {
		Optional<Produto> oProduto = produtoRepository.findById(id);
		
		if(oProduto != null && oProduto.isPresent()) {
			Produto produto = oProduto.get();
			ProdutoDTO produtoDTO = new ProdutoDTO(produto.getId(), produto.getNome(), produto.getDescricao(), produto.getValor(), produto.getEstoque(), produto.getFornecedor());
			return produtoDTO;
		} else {
			return null;
		}
		
	}
	
	//POST
	public ProdutoDTO saveProduto(ProdutoDTO produtoDTO) {
		Produto produto = new Produto(produtoDTO.getId(), produtoDTO.getNome(), produtoDTO.getDescricao(), produtoDTO.getValor(), produtoDTO.getEstoque(), produtoDTO.getFornecedor());
		produto = produtoRepository.save(produto);
		produtoDTO.setId(produto.getId());
		return produtoDTO;
	}
	
	public void delete(Long id) {
		produtoRepository.deleteById(id);
	}
	
	public List<ProdutoDTO> findAll() {
		List<ProdutoDTO> list = new ArrayList<>();
		Iterable<Produto> iProduto = produtoRepository.findAll();
		for(Produto produto : iProduto) {
	    ProdutoDTO produtoDTO = new ProdutoDTO(produto.getId(), produto.getNome(), produto.getDescricao(), produto.getValor(), produto.getEstoque(), produto.getFornecedor());
		list.add(produtoDTO);
		}
		return list;
		
	}
	
	public ProdutoDTO editProduto (Long id) {
		ProdutoDTO produtoDTO = new ProdutoDTO();
		Produto produto = new Produto(produtoDTO.getId(), produtoDTO.getNome(), produtoDTO.getDescricao(), produtoDTO.getValor(), produtoDTO.getEstoque(), produtoDTO.getFornecedor());
		produto = produtoRepository.save(produto);
		return produtoDTO;
	}

	
	
}
