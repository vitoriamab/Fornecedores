package fornecedores.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fornecedores.dto.ProdutoDTO;
import fornecedores.service.ProdutoService;

@RestController
@RequestMapping(value = "/produto")
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;
	
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<ProdutoDTO> findProdutoById(@PathVariable Long id){
		ProdutoDTO produtoDTO = produtoService.findById(id);
		if(produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<ProdutoDTO> saveProduto(@RequestBody ProdutoDTO produtoDTO){
		produtoDTO = produtoService.saveProduto(produtoDTO);
		if(produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<ProdutoDTO> delete(Long id){
		produtoService.delete(id);
		return new ResponseEntity<ProdutoDTO>(HttpStatus.OK);
		
	}
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoDTO>> findAll(){
		List<ProdutoDTO> produtoDTO = produtoService.findAll();
		if(produtoDTO != null) {
			return new ResponseEntity<List<ProdutoDTO>>(produtoDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<List<ProdutoDTO>>(produtoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.PUT)
	public ResponseEntity<ProdutoDTO> editProduto(@RequestBody ProdutoDTO produtoDTO){
		produtoDTO = produtoService.saveProduto(produtoDTO);
		if(produtoDTO != null) {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<ProdutoDTO>(produtoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
