package fornecedores.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fornecedores.dto.PedidoDTO;
import fornecedores.service.PedidoService;

@RestController
@RequestMapping(value = "/pedido")
public class PedidoControler {

	@Autowired
	private PedidoService pedidoService;
	
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<PedidoDTO> findPedidoById(@PathVariable Long id){
		PedidoDTO pedidoDTO = pedidoService.findById(id);
		if(pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.POST)
	public ResponseEntity<PedidoDTO> savePedido(@RequestBody PedidoDTO pedidoDTO){
		pedidoDTO = pedidoService.savePedido(pedidoDTO);
		if(pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<PedidoDTO> delete(Long id){
		pedidoService.delete(id);
		return new ResponseEntity<PedidoDTO>(HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.PUT)
	public ResponseEntity<PedidoDTO> editProduto(@RequestBody PedidoDTO pedidoDTO){
		pedidoDTO = pedidoService.savePedido(pedidoDTO);
		if(pedidoDTO != null) {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<PedidoDTO>(pedidoDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
