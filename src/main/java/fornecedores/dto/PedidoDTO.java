package fornecedores.dto;
import fornecedores.model.Cliente;
import fornecedores.model.Produto;

public class PedidoDTO {

	private long id;
	private String Descricao;
	Produto produto;
	Cliente cliente;
	private int qtd;
	private double valorTotal;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public int getQtd() {
		return qtd;
	}
	public void setQtd(int qtd) {
		this.qtd = qtd;
	}
	public double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}
	public PedidoDTO(long id, String descricao, Produto produto, Cliente cliente, int qtd, double valorTotal) {
		super();
		this.id = id;
		Descricao = descricao;
		this.produto = produto;
		this.cliente = cliente;
		this.qtd = qtd;
		this.valorTotal = valorTotal;
	}
	public PedidoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
