package fornecedores.dto;

import fornecedores.model.Fornecedor;

public class ProdutoDTO {

	private Long id;
	private String nome;
	private String descricao;
	private double valor;
	int estoque;
	Fornecedor fornecedor;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public Fornecedor getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}
	
	public int getEstoque() {
		return estoque;
	}
	public void setEstoque(int estoque) {
		this.estoque = estoque;
	}
	public ProdutoDTO(Long id, String nome, String descricao, double valor, int estoque, Fornecedor fornecedor) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.valor = valor;
		this.fornecedor = fornecedor;
		this.estoque=estoque;
	}
	
	public ProdutoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}
