package fornecedores.repository;

import org.springframework.data.repository.CrudRepository;

import fornecedores.model.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Long> {

}
