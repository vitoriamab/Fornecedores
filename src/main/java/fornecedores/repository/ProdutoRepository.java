package fornecedores.repository;

import org.springframework.data.repository.CrudRepository;

import fornecedores.model.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Long> {

}
